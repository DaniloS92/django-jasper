from django.conf.urls import url

from jasper.views import download_report

urlpatterns = [
    url(r'^download_report/', download_report, name='download_report_jasper')
]
