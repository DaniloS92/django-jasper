from django.shortcuts import HttpResponse


def download_report(request):
    format_report = request.GET.get('format')
    filename = request.GET.get('file_name') + "." + format_report
    file = request.GET.get('file') + "." + format_report
    filecontent = open(file, "rb").read()

    if not filecontent:
        raise Exception('Report not generated')
    else:
        response = HttpResponse(filecontent, content_type='application/{}'.format(format_report))
        response['Content-Disposition'] = 'attachment; filename={}'.format(filename)
        response['Set-Cookie'] = 'fileDownload=true; path=/'
        return response
