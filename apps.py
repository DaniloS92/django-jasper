from django.apps import AppConfig


class JasperConfig(AppConfig):
    name = 'jasper'
