import logging
import tempfile
from datetime import datetime
from django.urls import reverse

from django.conf import settings
from pyreportjasper.jasperpy import JasperPy

from django.shortcuts import redirect

_logger = logging.getLogger(__name__)


def generate_report(report_name, jasper_params, format_report, file_name, name_conection='default'):
    """
    Generate reports and downloads
    :param report_name: Comoiled report name (*.jasper)
    :param jasper_params: Params to send reports
    :param format_report: [xls, pdf]
    :param file_name: Name of file
    :param name_conection: Name of conection of database in settings django
    :return: Download report
    """

    _logger.info("init generate report")
    report_dir = settings.JASPER_REPORT_PATH
    input_file = report_dir + report_name
    jasper = JasperPy()
    output = tempfile.gettempdir() + "/" + file_name + "_" + str(datetime.now().microsecond)

    con = {
        'driver': 'postgres',
        'username': settings.DATABASES[name_conection]['USER'],
        'password': settings.DATABASES[name_conection]['PASSWORD'],
        'host': settings.DATABASES[name_conection]['HOST'],
        'database': settings.DATABASES[name_conection]['NAME'],
        'port': settings.DATABASES[name_conection]['PORT'],
        'schema': 'public',
    }

    try:
        _logger.info("start proces report")

        jasper.process(
            input_file, output_file=output,
            parameters=jasper_params,
            db_connection=con,
            format_list=[format_report],
            locale='en_US')

        _logger.info("end process report")
    except Exception as e1:
        _logger.error(e1)
        raise Exception('Please review the jrxml reports . {error}'.format(error=e1.args))
    _logger.info("end generate report")
    url = "{}?file_name={}&file={}&format={}".format(reverse('download_report_jasper'),
                                                     file_name, output, format_report)
    return redirect(url)
